import numpy


class DataGenerator:

    def __init__(self, vector_size, stop_flag):
        self.stop_flag = stop_flag
        self.sample_size = vector_size
        self.rng = numpy.random.default_rng()

    def generate(self):
        while not self.stop_flag.is_set():
            # TODO use dataclass to create event entity {event_name: "...", data: [....]}
            #  instead of generating plain list
            yield list(self.rng.standard_normal(self.sample_size))



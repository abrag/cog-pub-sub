import logging
from multiprocessing import Process
from queue import Queue

from publisher.data_generator import DataGenerator
from publisher.event_dropper import EventDropper
from publisher.rate_limiter import RateLimiter

logger = logging.getLogger(__name__)


class Publisher(Process):

    def __init__(self,
                 data_generator: DataGenerator,
                 event_rate_limiter: RateLimiter,
                 event_dropper: EventDropper,
                 event_queue: Queue
                 ):
        super().__init__()
        self.queue = event_queue
        self.dropper = event_dropper
        self.data_generator = data_generator
        self.rate_limiter = event_rate_limiter

    def run(self):
        logger.info("Publisher started")
        try:
            with self.rate_limiter as rate_limiter:
                for datum in self.data_generator.generate():
                    rate_limiter.rate_limit()
                    self.dropper.drop(self.queue.put, datum)
        except KeyboardInterrupt:
            exit(0)
        finally:
            logger.info("Publisher exiting")


import threading
import time


# TODO: Rate limiting function can be tweaked to get more uniform event publishing
class RateLimiter:
    def __init__(self, limit, every=1):
        self.semaphore = None
        self.every = every
        self.count = 0
        self.should_run = False
        self.limit = limit
        self.limiter_thread = limit

    def _reset_rate_limiter_timer(self):
        if self.should_run:
            if self.count:
                self.semaphore.release(self.count)
            self.count = 0
            timer = threading.Timer(self.every, self._reset_rate_limiter_timer)
            timer.setDaemon(True)
            timer.start()

    def _reset_rate_limiter_thread(self):
        def reset():
            while self.should_run:
                time.sleep(self.every)
                if self.count:
                    self.semaphore.release(self.count)
                self.count = 0

        self.limiter_thread = threading.Thread(target=reset)
        self.limiter_thread.setDaemon(True)
        self.limiter_thread.start()

    def __enter__(self):
        self.semaphore = threading.Semaphore(self.limit)
        self.should_run = True
        self._reset_rate_limiter_timer()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.should_run = False

    def rate_limit(self):
        self.semaphore.acquire()
        self.count += 1

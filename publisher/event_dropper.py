import logging
import time
from enum import Enum

import numpy

logger = logging.getLogger(__name__)


class EventDropperType(Enum):
    NONE = 1
    Unifrom = 2


class EventDropper:
    def should_drop(self):
        return False

    def drop(self, event_dispatcher, event):
        if self.should_drop():
            return
        return event_dispatcher(event)


class UniformEventDropper(EventDropper):

    def __init__(self, low, high):
        self.high = high
        self.low = low
        self.next_drop_in_sec = 0
        self.start = self.start = time.perf_counter()
        self.rng = numpy.random.default_rng()

    def next_drop_time_generator(self):
        return self.rng.integers(low=self.low * 1000, high=self.high * 1000, endpoint=True) / 1000

    def should_drop(self):
        now = time.perf_counter()
        if now >= self.start + self.next_drop_in_sec:
            self.next_drop_in_sec = self.next_drop_time_generator()
            self.start = now
            logger.debug("dropped")
            return True
        return False


def factory(event_dropper_type: EventDropperType, *args, **kwargs):
    if event_dropper_type == EventDropperType.NONE:
        return EventDropper()
    else:
        return UniformEventDropper(*args, **kwargs)

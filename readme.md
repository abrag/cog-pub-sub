# Simulator

A python 3.9  application 

Rate limiting and rate calculation to 1000hz is a bit problematic its depending on locking or waiting for thread to wake up (sleep) which leads to race conditions

please give the app few sec before the rates are stable 

(you may see a rate abit above the 1000hz due to race conditions between processes, Subscriber Process can check rates before Publisher published)

## running locally

Install pipenv 

```bash
pipenv install
pipenv shell

# help command:
python application.py --help

# add noise:
python application.py --noise

# add timeout (app will stop after timeout elapsed:
python application.py --timeout 60
```

## running on docker

install Docker 
to get results out of the docker, you need to mount a volume and specify the output file (see example below)

```bash
docker build . -t simulator_app:0.0.1
# help command:
docker run -it --rm simulator_app:0.0.1 --help

#timeout + noise
docker run -it --rm simulator_app:0.0.1 --timeout 30 --noise

#run till ctrl-c pressed (+ saving result file on our local machine)
docker run -it --rm --volume /tmp:/tmp/output simulator_app:0.0.1 --output-file /tmp/output/output.csv
```

## notes
- I used pub/sub like pattern
- for 1000 events per second (1000Hz) its enough to have 1 thread to generate data and 1 to accumulate it  
- the code can be easily changed to add more producers/consumer threads
- ToDo: use dataclasses 
    * instead of plain vector return an event object `{event_name: str, data: []}`
    * instead of aggregated_data list `[aggregation-name, time_since_epoc_in_ms, mean-x1,..., mean-xn, std-x1,...xn]` return an object `{name, time, mean:list[], std:list[]}`
- Rate limiting and rate calculation to 1000hz is a bit problematic its depending on locking or waiting for thread to wake up (sleep) which leads to race conditions
- please give the app few sec before the rates are stable
- I used the mean calculation to display warning if the acquisition rate is lower than it should (and to adjust the warnings to the incoming rate), I can easily change it to a const of 1000Hz
- Rate limiting function can be tweaked to get more uniform event publishing 
## output file
 a csv of 
`[aggregation_name, time_since_epoc_in_ms, mean-x1,..., mean-xn, std-x1,...,std-xn]`,  
 or  `[aggregation_name, time_since_epoc_in_ms, Hz]`
 (for real life usage the monitoring stats and the analytical stats should be saved to different files)   
csv are good for later processing this data via different tools

example file:
```
data_analytics,1666128136953,-0.0135135368244396,0.15867634340802456,-0.14165398900412332,-0.03254459945840496,0.022200422356852965,-0.07727515934154149,-0.011122421160672961,-0.11662154353806412,-0.06533970977604112,-0.032821553420930745,-0.08026416830363926,0.07879717656661012,0.019201147393981002,-0.11493388060433236,0.08058291901927692,0.19038618941326962,-0.05926729773258824,0.016822335962894924,0.012200521234819316,0.02610162249774563,0.05708677751635499,-0.02087838407489583,-0.04930842627030847,0.15388385187817813,-0.10267404035015211,-0.030112790718253648,0.07895558829720366,0.09907713213016896,0.038454714369162785,0.011491428957516609,0.0715783077145048,0.22238039173270796,-0.12676076353642776,0.17624675044745147,0.09629032448818821,0.004680600712008693,0.08320157448094623,-0.025730017736761365,-0.031771578776199044,0.013711158618338382,-0.08985246707541014,0.08083803076496097,-0.03031234218098626,-0.044341861335963,0.05628928867554736,-0.09071647535657902,0.059047424202867765,-0.02870121234402712,-0.09972741778832246,0.008039867649262507,1.027966178467557,1.0394873467957386,1.016103592599089,1.015821257502907,0.9276534815293679,1.0093307699802754,0.9749869800569045,1.0320321209213439,0.98119120093432,0.999580162813204,0.978846722985989,0.9531291249323983,0.9091814696563518,1.022144022938185,1.021634525492391,1.0579300556540998,0.9469012262278047,1.02614649661541,1.0198682650491064,0.849301812984953,1.0214831944745026,0.9636007779663928,0.8667730396453028,0.944328585955845,1.0254965276312726,0.9767053478731119,1.0522229838049777,0.9895065056334503,1.031553056475196,0.8960256919056313,1.1297122368627046,0.7915600921547122,0.9252859487989253,1.0182011265791953,1.0199832169889136,0.9985985644605508,0.9592165523239662,0.9039983899162023,1.082765433716327,0.9710324198044024,0.9494309876534991,0.9408104170871037,0.9234709348269047,0.9644759240198961,0.9975092476407248,1.0793741971695952,1.025549755831003,1.074048174030223,0.9152686203734297,1.0646065440270984
acquisition_rates_analytics,1666172479449,669.0,473.0546127739023
acquisition_rates,1666172510573,1004.0
```

**data_analytics** record:
```
column 1     : name
column 2     : timestamp
column 3-52  : mean for each of the x1...x50 data points in the vector
column 53-102: std for each of the x1...x50 data points in the vector
```
**acquisition_rates_analytics** record:
```
column 1      : name
column 2      : timestamp
column 3      : mean for the rate
column 4      : std for the rate
```
**acquisition_rates** record:
```
column 1      : name
column 2      : timestamp
column 3      : calculated rate
```


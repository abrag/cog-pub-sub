FROM python:3.9
WORKDIR /opt/simulator/
COPY . /opt/simulator/

RUN pip install -U pip

RUN pip install -r requirements.txt

ENTRYPOINT ["python", "./application.py"]

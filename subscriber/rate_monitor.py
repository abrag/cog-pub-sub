import functools
import logging
import threading
import time

from subscriber.aggregations import SimpleAggregator

logger = logging.getLogger(__name__)


class RateMonitor:
    def __init__(self, acquisition_rates_analytics_aggregator: SimpleAggregator, every, monitoring_name, acquisition_rates_aggregator: SimpleAggregator):
        self.monitoring_name = monitoring_name
        self.every = every
        self.aggregator = acquisition_rates_analytics_aggregator
        self.counter = 0
        self.prev_counter_value = 0
        self.running = False
        self.aggregated_value = [0, 0, 0, 0]
        self.rates_aggregator = acquisition_rates_aggregator

    def increment(self, value=1):
        self.counter += value

    def increment_decorator(self, fn):
        @functools.wraps(fn)
        def wrapper(*args, **kwargs):
            res = fn(*args, **kwargs)
            self.increment()
            return res
        return wrapper

    def __enter__(self):
        self.running = True
        self._calc_and_display_stats()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.running = False

    def _calc_and_display_stats(self):
        if self.running:
            timer = threading.Timer(self.every, self._calc_and_display_stats)
            timer.setDaemon(True)
            timer.start()
        local_counter = self.counter
        rps = ((local_counter - self.prev_counter_value) / self.every)
        self.rates_aggregator.aggregate(rps)
        aggregated_value = self.aggregator.aggregate([rps])
        if aggregated_value:
            if self.aggregated_value[2] != aggregated_value[2]:
                logger.info("%s ==> New events per second baseline: %s, std: %s", self.monitoring_name, aggregated_value[2],
                               aggregated_value[3])
            self.aggregated_value = aggregated_value

        msg = f"{self.monitoring_name} ==> count: {local_counter - self.prev_counter_value}, elapsed time: {self.every}sec ,Hz: {rps:0.4f}"
        if self.aggregated_value and self.aggregated_value[2] > rps:
            logger.warning(msg)
        else:
            logger.debug(msg)

        self.prev_counter_value = local_counter



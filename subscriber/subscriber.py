import logging

from multiprocessing import Process, Queue, Event
from queue import Empty

from subscriber.aggregations import SimpleAggregator
from subscriber.rate_monitor import RateMonitor

logger = logging.getLogger(__name__)


class Subscriber(Process):
    def __init__(self,
                 writer,
                 data_analytics_aggregator: SimpleAggregator,
                 rate_monitor: RateMonitor,
                 event_queue: Queue,
                 exit_event: Event):
        super().__init__()
        self.writer = writer
        self.events_queue = event_queue
        self.exit_event = exit_event
        self.aggregator = data_analytics_aggregator
        self.rate_monitor = rate_monitor

    def run(self):
        logger.info("Subscriber started")

        @self.rate_monitor.increment_decorator
        def work():
            try:
                aggregated_value = self.aggregator.aggregate(self.events_queue.get(timeout=0.2))
                if aggregated_value:
                    writer.writerow(aggregated_value)
            except Empty:
                return

        try:
            with self.writer as writer:
                with self.rate_monitor:
                    try:
                        while not self.exit_event.is_set():
                            work()
                    finally:
                        # can also use ThreadSafeCSVWriter to delegate the writes to file to another thread
                        # TODO:
                        #  option: we can change the code easily to make both data_analytics & acquisition_rates
                        #  to be written continuously to same file or to different files by passing
                        #  ThreadSafeCSVWriter to the Aggregator class for the writes
                        for stats in self.rate_monitor.aggregator.get_aggregation_output():
                            writer.writerow(stats)
                        for rate in self.rate_monitor.rates_aggregator.get_aggregation_output():
                            writer.writerow(rate)
        except KeyboardInterrupt:
            exit(0)
        finally:
            logger.info("Subscriber exiting")

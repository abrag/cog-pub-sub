import csv


class CSVWriter:
    def __init__(self, filename):
        self.filename = filename
        self.fp = None

    def __enter__(self):
        # todo: return self and add writerow func
        #  also maybe adding flush
        self.fp = open(self.filename, 'w')
        return csv.writer(self.fp, delimiter=',')

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.fp.close()

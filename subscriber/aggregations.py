import time

import numpy


class SimpleAggregator:
    def __init__(self, aggregation_name):
        self.aggregation_name = aggregation_name
        self.result = []

    def aggregate(self, datum):
        analytics = [self.aggregation_name, time.time_ns() // 1000000, datum]
        self.result.append(analytics)
        return analytics

    def get_aggregation_output(self):
        return self.result


class VectorAnalyticalAggregator(SimpleAggregator):
    def __init__(self, aggregation_size, aggregation_name):
        super().__init__(aggregation_name)
        self.aggregation_size = aggregation_size
        self.aggregation = []

    def aggregate(self, datum):
        self.aggregation.append(datum)
        if len(self.aggregation) % self.aggregation_size == 0:
            matrix = numpy.matrix(self.aggregation)
            # TODO: use dataclass create entity for aggregated_data {name, time, mean:list[], std:list[]},
            #  current output is a vector of
            #  (aggregation-name, time_since_epoc_in_ms, mean-x1,..., mean-xn, std-x1,...xn)
            analytics = [self.aggregation_name, time.time_ns() // 1000000] \
                        + list(matrix.mean(axis=0).A1) \
                        + list(matrix.std(axis=0).A1)
            self.result.append(analytics)
            self.aggregation.clear()
            return analytics

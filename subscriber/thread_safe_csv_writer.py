import csv
import logging
import threading
from queue import Queue, Empty
from typing import Iterable, Any, Union

logger = logging.getLogger(__name__)


class ThreadSafeCSVWriter:
    def __init__(self, filename):
        self.filename = filename
        self.fp = None
        self.queue: Union[Queue, None] = None
        self.writer_thread = None
        self.running = False

    def __enter__(self):
        self.queue = Queue()
        self.running = True
        self.fp = open(self.filename, 'w')

        self.writer_thread = threading.Thread(target=self._writerows)
        self.writer_thread.setDaemon(True)
        self.writer_thread.start()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.running = False
        self.writer_thread.join()
        self.fp.close()
        self.queue = None

    def _writerows(self):
        writer = csv.writer(self.fp, delimiter=',')
        while self.running or not self.queue.empty():
            try:
                writer.writerow(self.queue.get(timeout=.3))
            except Empty:
                continue
        logger.info("CSVWriter finished")

    def writerow(self, row: Iterable[Any]):
        self.queue.put(row)

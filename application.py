import argparse
import logging
import time
from multiprocessing import Queue, Event

import colorlog

from publisher import event_dropper
from publisher.data_generator import DataGenerator
from publisher.publisher import Publisher
from publisher.rate_limiter import RateLimiter
from subscriber.subscriber import Subscriber
from subscriber.rate_monitor import RateMonitor
from subscriber.aggregations import VectorAnalyticalAggregator, SimpleAggregator
from subscriber.thread_safe_csv_writer import ThreadSafeCSVWriter
from subscriber.csv_writer import CSVWriter

formatter = colorlog.ColoredFormatter()
handler = colorlog.StreamHandler()
handler.setFormatter(colorlog.ColoredFormatter('%(asctime)s | %(name)s | %(log_color)s%(levelname)s | %(message)s'))
logging.basicConfig(level=logging.DEBUG, handlers=[handler])
# handler.setFormatter(formatter)
# logging.basicConfig(level=logging.DEBUG, format='%(asctime)s | %(levelname)s | %(name)s | %(message)s')


def set_events(events):
    for e in events:
        e.set()


def wait_and_exit(processes):
    for p in processes:
        p.join()
    logger.info("Application exiting")


if __name__ == '__main__':
    logger = logging.getLogger(__name__)

    parser = argparse.ArgumentParser(description='vector simulation tool')
    parser.add_argument('--noise', default=False, action=argparse.BooleanOptionalAction, help="vector dropping")
    parser.add_argument('--output-file', default="output.csv", type=str, help="file path for the results")
    parser.add_argument('--timeout', default=None, type=int,
                        help="stopping simulation after timeout (sec) elapsed, if not specified stop with ctrl-c")
    args = parser.parse_args()

    logger.info("Application started, Press ctrl-c to stop")

    processes = []
    exit_events = []
    try:
        queue = Queue()

        publisher_exit_event = Event()
        subscriber_exit_event = Event()
        ed = event_dropper.factory(event_dropper.EventDropperType.Unifrom, low=2, high=3) if args.noise else \
            event_dropper.factory(event_dropper.EventDropperType.NONE)

        publisher = Publisher(
            data_generator=DataGenerator(vector_size=50, stop_flag=publisher_exit_event),
            event_rate_limiter=RateLimiter(limit=1000, every=1),
            event_dropper=ed,
            event_queue=queue
        )

        subscriber = Subscriber(
            writer=CSVWriter(filename=args.output_file),
            data_analytics_aggregator=VectorAnalyticalAggregator(aggregation_size=100, aggregation_name="data_analytics"),
            rate_monitor=RateMonitor(
                acquisition_rates_aggregator=SimpleAggregator("acquisition_rates"),
                every=1,  # in msec
                monitoring_name="acquisition_rates",
                acquisition_rates_analytics_aggregator=VectorAnalyticalAggregator(aggregation_size=3, aggregation_name="acquisition_rates_analytics")
            ),
            event_queue=queue,
            exit_event=subscriber_exit_event
        )

        processes.append(publisher)
        processes.append(subscriber)
        exit_events.append(publisher_exit_event)
        exit_events.append(subscriber_exit_event)
        publisher.start()
        subscriber.start()

        if args.timeout:
            time.sleep(args.timeout)
            set_events(exit_events)

        wait_and_exit(processes)
    except KeyboardInterrupt:
        set_events(exit_events)
        wait_and_exit(processes)
